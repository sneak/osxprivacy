AUTODMG := /Applications/AutoDMG.app/Contents/MacOS/AutoDMG

default: output.dmg

.PHONY: prepare

packages:
	cd osx-privacy-pkg && make

clean:
	rm -f osx-privacy-pkg/*.pkg output.dmg

prepare:
	sudo xcodebuild -license accept
	sudo pmset -a sleep 180
	sudo pmset -a displaysleep 180
	$(AUTODMG) update

output.dmg: packages prepare
	-$(AUTODMG) \
		--log-level 7 \
		--logfile - \
		build \
		-n "root" \
		-u -U \
		-o /tmp/output.dmg \
		"/Applications/Install OS X El Capitan.app" \
		/Applications/Chromium.app \
		/Applications/Xcode.app \
		/Applications/AutoDMG.app \
		/Applications/Little\ Snitch\ Installer.app \
		"/Applications/Install OS X El Capitan.app" \
		$(PWD)/../extrapackages/*.pkg \
		$(PWD)/users/*.pkg \
		$(PWD)/clearRegistration.pkg \
		$(PWD)/osx-privacy-pkg/*.pkg && \
		cp /tmp/output.dmg $(PWD)/$@

simple.dmg: prepare
	$(AUTODMG) \
		--log-level 7 \
		build \
		-n "root" \
		-u -U \
		-o /tmp/output.dmg \
		"/Applications/Install OS X El Capitan.app" \
		/Applications/Chromium.app \
		/Applications/Xcode.app \
		/Applications/AutoDMG.app \
		/Applications/Little\ Snitch\ Installer.app \
		"/Applications/Install OS X El Capitan.app" \
		$(PWD)/../extrapackages/*.pkg \
		$(PWD)/users/*.pkg \
		$(PWD)/clearRegistration.pkg \
		&& cp /tmp/output.dmg $(PWD)/$@
